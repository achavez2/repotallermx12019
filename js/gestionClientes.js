var clientesObtenidos;

function getClientes(){
  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var request = new XMLHttpRequest();
  request.onreadystatechange = function(){
    if(this.readyState == 4 && this.status == 200){
      //console.log(request.responseText);
      clientesObtenidos = request.responseText;
      procesarClientes();
    }
  }

  request.open("GET", url, true);
  request.send();
}

function procesarClientes() {
  var JSONProductos = JSON.parse(clientesObtenidos);
  //alert(JSONProductos.value[0].ProductName);
  var divTabla = document.getElementById("tablaClientes");
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");

  tabla.classList.add("table");
  tabla.classList.add("table-striped");

  for (var i = 0; i < JSONProductos.value.length; i++) {
    //console.log(JSONProductos.value[i].ProductName);
    var nuevaFila  = document.createElement("tr");

    var columnaNombre  = document.createElement("td");
    columnaNombre.innerText = JSONProductos.value[i].ContactName;

    var columnaCity  = document.createElement("td");
    columnaCity.innerText = JSONProductos.value[i].City;

    var columnaCountry = document.createElement("td");

    var img=document.createElement('img');
    img.classList.add("flag");

    if(JSONProductos.value[i].Country == "UK"){
      img.setAttribute("src","https://www.countries-ofthe-world.com/flags-normal/flag-of-"+"United-Kingdom"+".png");
    }else{
      img.setAttribute("src","https://www.countries-ofthe-world.com/flags-normal/flag-of-"+JSONProductos.value[i].Country+".png");
    }
    //img.setAttribute("width","70");
    //img.setAttribute("height","42");

    //luegos metes img en el contenedor
    columnaCountry.appendChild(img);

    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaCity);
    nuevaFila.appendChild(columnaCountry);

    tbody.appendChild(nuevaFila);
  }

  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);
}
